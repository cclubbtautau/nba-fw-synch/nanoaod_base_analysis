.. _info:

============
More info
============

Useful commands and other info: https://docs.google.com/document/d/12xdRpw38jcN4z_wfIX0wrc4c88v6h2dnOVW_KjDJocw/edit#

Slides of a hands-on tutorial (information about existing RDF modules): https://cernbox.cern.ch/index.php/s/tGt5S3iuopmjnUk
