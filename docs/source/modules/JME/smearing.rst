Smearing
==========

.. currentmodule:: Corrections.JME.smearing

.. contents::

.. _JME_smearing_jetSmearerRDF:

``jetSmearerRDF``
-----------------

.. autoclass:: jetSmearerRDF
   :members:

``jetVarRDF``
-------------

.. autoclass:: jetVarRDF
   :members:

``metSmearerRDF``
-----------------

.. autoclass:: metSmearerRDF
   :members:

