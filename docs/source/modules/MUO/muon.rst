muon SF
=======

.. currentmodule:: Corrections.MUO.muCorrections

.. contents::

.. _Muon_muSFRDF:

``muSFRDF``
------------------------------

.. autoclass:: muSFRDF
   :members:

